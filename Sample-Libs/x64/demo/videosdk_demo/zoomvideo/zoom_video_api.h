#pragma once

#ifdef ZOOM_VIDEO_EXPORT
	#define ZOOM_VIDEO_API __declspec(dllexport)
#else
	#define ZOOM_VIDEO_API __declspec(dllimport)
#endif

extern "C" {
	namespace zoom_video_api {
		/// \brief Start Zoom service
		ZOOM_VIDEO_API bool start();

		/// \brief Join Zoom session
		ZOOM_VIDEO_API bool join(const wchar_t* user_name, const wchar_t* session_name, const wchar_t* session_password, const wchar_t* jwt);

		/// \brief Leave Zoom session
		ZOOM_VIDEO_API void leave();

		/// \brief Stop zoom service
		ZOOM_VIDEO_API void stop();
	}
}
