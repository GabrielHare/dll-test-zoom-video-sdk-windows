#include "zoom_video_api.h"

#include "videosdk_demo_mgr.h"
#include "zoom_video_sdk_delegate.h"
#include "zoom_video_raw_data_pipe_delegate.h"

#include <vector>

namespace  {
	using namespace zoom_video_api;
	
	// TODO: Device configuration should accept name as parameter, and should select best match
	
	void configure_camera() {
		auto manager = ZoomVideoSDKMgr::GetInst();
		auto camera_list = manager.GetCameraList();
		if(!camera_list) return;

		IZoomVideoSDKCameraDevice* selected_camera = nullptr;
		for(int i = 0; i < camera_list->GetCount(); ++i) {
			auto camera = camera_list->GetItem(i);
			if(!camera) continue;

			// NOTE: Initialization invokes this for every device
			camera->isSelectedDevice();

			// TEMP: Select the first device
			if(!selected_camera) selected_camera = camera;
		}

		if(selected_camera) {
			manager.SelectCamera(selected_camera->getDeviceId());
			std::wcout << "zoom_video_api: using camera '" << selected_camera->getDeviceName() << "'" << std::endl;
		}
	}

	void configure_speaker() {
		auto manager = ZoomVideoSDKMgr::GetInst();
		auto speaker_list = manager.GetSpeakerList();
		if(!speaker_list) return;

		IZoomVideoSDKSpeakerDevice* selected_speaker = nullptr;
		for(int i = 0; i < speaker_list->GetCount(); ++i) {
			auto speaker = speaker_list->GetItem(i);
			if(!speaker) continue;

			// NOTE: Initialization invokes this for every device
			speaker->isSelectedDevice();

			// TEMP: Select the first device
			if(!selected_speaker) selected_speaker = speaker;
		}

		if(selected_speaker) {
			manager.SelectCamera(selected_speaker->getDeviceId());
			std::wcout << "zoom_video_api: using speaker '" << selected_speaker->getDeviceName() << "'" << std::endl;
		}
	}

	void configure_mic() {
		auto manager = ZoomVideoSDKMgr::GetInst();
		auto mic_list = manager.GetMicList();
		if(!mic_list) return;

		IZoomVideoSDKMicDevice* selected_mic = nullptr;
		for(int i = 0; i < mic_list->GetCount(); ++i) {
			auto mic = mic_list->GetItem(i);
			if(!mic) continue;

			// NOTE: Initialization invokes this for every device
			mic->isSelectedDevice();

			// TEMP: Select the first device
			if(!selected_mic) selected_mic = mic;
		}

		if(selected_mic) {
			manager.SelectCamera(selected_mic->getDeviceId());
			std::wcout << "zoom_video_api: using mic '" << selected_mic->getDeviceName() << "'" << std::endl;
		}
	}

	// TODO: receiver list should be maintained where user join/leave callbacks are received
	//auto receiver_list = std::map<IZoomVideoSDKUser*, zoom_video_raw_data_pipe_delegate>();

	auto receiver = zoom_video_raw_data_pipe_delegate();
	
	void SubscribeAllUser()
	{
		std::vector<IZoomVideoSDKUser*> user_list = ZoomVideoSDKMgr::GetInst().GetAllUsers();
		std::wcout << "zoom_video: subscribing " << user_list.size() << " users" << std::endl;
		for (size_t i = 0; i < user_list.size(); i++)
		{
			auto user = user_list[i];
			if (!user) continue;

			std::wcout << "zoom_video: subscribing user '" << user->getUserName() << "'" << std::endl;
			
			// WARNING: If a heap allocated receiver instance is used the program will eventually terminate with exit code:
			// -1073740791 = 0xC0000409
			// This is a fast-fail exception that cannot be caught
			// SOLUTION: Use a stack allocated instance.
			
			// QUESTION: Should there be a check to avoid re-subscribing?
			receiver.Subscribe(user, RAW_DATA_TYPE_VIDEO, ZoomVideoSDKResolution_1080P);
		}
	}

	void UnSubscribeAllUser()
	{
		receiver.unSubscribe();
		receiver.Clean();
	}
}

namespace zoom_video_api {
	bool start() {
		zoom_video_sdk_delegate::GetInstance().InitVideoSDK();

		// TODO: Get initialization success
		
		return true;
	}

	bool join(const wchar_t* user_name, const wchar_t* session_name, const wchar_t* session_password, const wchar_t* jwt) {
		// TEMP: Configure devices
		// OBSERVATION: This is done BEFORE joining session
		configure_camera();
		configure_speaker();
		configure_mic();
		
		// Session identification
		ZoomVideoSDKSessionContext session_context;
		session_context.sessionName = session_name;
		session_context.sessionPassword = session_password;
		session_context.userName = user_name;

		// JWT for this session.
		// WARNING: Session name must match the name used when constructing jwt
		// TODO: It would be better to construct jwt here to avoid mismatch errors
		session_context.token = jwt;
		
		// Video and audio initialization
		session_context.videoOption.localVideoOn = true;
		session_context.audioOption.connect = true;
		session_context.audioOption.mute = false;

		std::wcout << "zoom_video_api: joining session with:\n"
		<< " - sessionName = '" << session_context.sessionName << "'\n"
		<< " - userName = '" << session_context.userName << "'\n"
		<< " - token = '" << session_context.token << "'" << std::endl;

		// NOTE: This begins the connection process
		// At this point the session will have no associated id
		zoom_video_sdk_delegate::GetInstance().JoinSession(session_context);
		// TODO: Get join success
		
		ZoomVideoSDKMgr::GetInst().UnmuteVideo();
		ZoomVideoSDKMgr::GetInst().UnmuteAudio();

		// NOTE: This is done after beginning connecting process
		SubscribeAllUser();
		
		// EXPECT: Callback to OnSessionJoined ... eventually
		
		return true;
	}

	void leave() {
		UnSubscribeAllUser();
		
		// NOTE: If argument was false host role would be transferred, if possible
		zoom_video_sdk_delegate::GetInstance().LeaveSession(true);
	}

	void stop() {
		zoom_video_sdk_delegate::GetInstance().UninitVideoSDK();
	}
}
