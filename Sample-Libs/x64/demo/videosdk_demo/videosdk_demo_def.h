#pragma once

#include "stdafx.h"

enum CameraControlAction
{
	TURN_LEFT, TURN_RIGHT, TURN_UP, TURN_DOWN, ZOOM_IN, ZOOM_OUT
};

enum CmdChannelType
{
	CMD_REACTION = 1, CMD_FEEDBACK_LAUNCH, CMD_FEEDBACK_SUBMIT, CMD_LOWER_THIRD
};

enum LowerThirdColor
{
	COLOR_NONE, COLOR_BULE, COLOR_VIOLET, COLOR_GREEN, COLOR_ORANGE, COLOR_RED, COLOR_DARKBULE, COLOR_YELLOW
};

struct LowerThirdInfo
{
	std::wstring lower_third_name;
	std::wstring lower_third_description;
	std::wstring lower_third_color;

	LowerThirdInfo()
	{
		lower_third_name = _T("");
		lower_third_description = _T("");
		lower_third_color = _T("");
	}
};

struct LowerThirdColorInfo
{
	LowerThirdColor color;
	std::wstring color_normal_image;
	std::wstring color_triangle_image;
	std::wstring bkcolor;
	Gdiplus::Color gdi_font_color;
	Gdiplus::Color gdi_font_bk_color;
	DWORD textcolor;

	LowerThirdColorInfo()
	{
		color = LowerThirdColor::COLOR_NONE;
		bkcolor = _T("#444B53");
		textcolor = 0XF7F9FA;
		color_normal_image = _T("res = 'BUTTON_COLOR_COMMON' restype = 'ZPIMGRES' source = '0,0,2,18'");
		color_triangle_image = _T("res = 'BUTTON_COLOR_COMMON' restype = 'ZPIMGRES' source = '0,0,11,18'");
		gdi_font_color = Gdiplus::Color(255, 255, 255);
		gdi_font_bk_color = { 68, 75, 83 };
	}
};

class IParseChannelCmdWnd {
public:
	virtual void OnParseChannelCmd(IZoomVideoSDKUser* pSender,const std::vector<std::wstring >& cmdVector) = 0;
	virtual HWND GetHWND() = 0;
};
