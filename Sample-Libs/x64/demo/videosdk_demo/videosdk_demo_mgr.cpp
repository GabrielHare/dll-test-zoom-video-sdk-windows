#include "videosdk_demo_mgr.h"
#include "videosdk_include_files.h"
#include "zoom_video_sdk_errors.h"

using namespace std;

#include <vector>
#include <string>

using namespace std;

ZoomVideoSDKMgr::ZoomVideoSDKMgr()
	: video_sdk_obj_(nullptr)
	, is_inited_(false)
{

}

ZoomVideoSDKMgr::~ZoomVideoSDKMgr()
{

}

ZoomVideoSDKMgr& ZoomVideoSDKMgr::GetInst()
{
	static ZoomVideoSDKMgr inst;
	return inst;
}

bool ZoomVideoSDKMgr::Init(IZoomVideoSDKDelegate* listener, ZoomVideoSDKInitParams init_params)
{
	if (!is_inited_)
	{
		video_sdk_obj_ = CreateZoomVideoSDKObj();

		if (video_sdk_obj_)
		{
			ZoomVideoSDKErrors err = video_sdk_obj_->initialize(init_params);

			if (ZoomVideoSDKErrors_Success == err)
			{
				is_inited_ = true;
				video_sdk_obj_->addListener(listener);
			}
		}
	}
	return is_inited_;
}

void ZoomVideoSDKMgr::UnInit()
{
	if (is_inited_)
	{
		if (video_sdk_obj_)
		{
			video_sdk_obj_->cleanup();
			DestroyZoomVideoSDKObj();
			video_sdk_obj_ = nullptr;
		}
		is_inited_ = false;
	}
}

IZoomVideoSDKSession* ZoomVideoSDKMgr::JoinSession(ZoomVideoSDKSessionContext& session_context)
{
	IZoomVideoSDKSession* session = nullptr;

	if (video_sdk_obj_)
	{
		session = video_sdk_obj_->joinSession(session_context);
	}

	return session;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::LeaveSession(bool end)
{
	if (video_sdk_obj_)
	{
		return video_sdk_obj_->leaveSession(end);
	}

	return ZoomVideoSDKErrors_Uninitialize;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::MuteAudio()
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKAudioHelper* audio_helper = video_sdk_obj_->getAudioHelper();
		if (audio_helper)
		{
			IZoomVideoSDKUser* my_self = GetMySelf();
			if (my_self)
			{
				return audio_helper->muteAudio(my_self);
			}
		}
	}
	return ZoomVideoSDKErrors_Uninitialize;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::UnmuteAudio()
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKAudioHelper* audio_helper = video_sdk_obj_->getAudioHelper();
		if (audio_helper)
		{
			IZoomVideoSDKUser* my_self = GetMySelf();
			if (my_self)
			{
				return audio_helper->unMuteAudio(my_self);
			}
		}
	}
	return ZoomVideoSDKErrors_Uninitialize;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::MuteVideo()
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKVideoHelper* video_helper = video_sdk_obj_->getVideoHelper();
		if (video_helper)
		{
			return video_helper->stopVideo();
		}
	}
	return ZoomVideoSDKErrors_Uninitialize;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::UnmuteVideo()
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKVideoHelper* video_helper = video_sdk_obj_->getVideoHelper();
		if (video_helper)
		{
			return video_helper->startVideo();
		}
	}
	return ZoomVideoSDKErrors_Uninitialize;
}

bool ZoomVideoSDKMgr::IsMyselfVideoOn()
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKUser* myself = GetMySelf();
		if (myself)
		{
			ZoomVideoSDKVideoStatus video_status = myself->getVideoStatus();
			if (video_status.isOn)
				return true;
			else
				return false;
		}
	}
	return false;
}

bool ZoomVideoSDKMgr::IsMyselfAudioMuted()
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKUser* myself = GetMySelf();
		if (myself)
		{
			ZoomVideoSDKAudioStatus audio_status = myself->getAudioStatus();
			if (audio_status.isMuted)
				return true;
			else
				return false;
		}
	}
	return false;
}

bool ZoomVideoSDKMgr::SelectCamera(const zchar_t* camera_device_id)
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKVideoHelper* video_helper = video_sdk_obj_->getVideoHelper();
		if (video_helper)
		{
			return video_helper->selectCamera(camera_device_id);
		}
	}
	return false;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::SelectSpeaker(const zchar_t* device_id, const zchar_t* device_name)
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKAudioHelper* audio_helper = video_sdk_obj_->getAudioHelper();
		if (audio_helper)
		{
			return audio_helper->selectSpeaker(device_id, device_name);
		}
	}
	return ZoomVideoSDKErrors_Uninitialize;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::SelectMic(const zchar_t* device_id, const zchar_t* device_name)
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKAudioHelper* audio_helper = video_sdk_obj_->getAudioHelper();
		if (audio_helper)
		{
			return audio_helper->selectMic(device_id, device_name);
		}
	}
	return ZoomVideoSDKErrors_Uninitialize;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::StartShareScreen(const zchar_t* monitorID, ZoomVideoSDKShareOption option)
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKShareHelper* share_helper = video_sdk_obj_->getShareHelper();
		if (share_helper)
		{
			return share_helper->startShareScreen(monitorID, option);
		}
	}
	return ZoomVideoSDKErrors_Uninitialize;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::StartShareView(void* hwnd, ZoomVideoSDKShareOption option)
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKShareHelper* share_helper = video_sdk_obj_->getShareHelper();
		if (share_helper)
		{
			return share_helper->startShareView(hwnd, option);
		}
	}
	return ZoomVideoSDKErrors_Uninitialize;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::StopShare()
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKShareHelper* share_helper = video_sdk_obj_->getShareHelper();
		if (share_helper)
		{
			return share_helper->stopShare();
		}
	}
	return ZoomVideoSDKErrors_Uninitialize;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::SendCommand(IZoomVideoSDKUser* receiver, const zchar_t* strCmd)
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKCmdChannel* cmd_channel = video_sdk_obj_->getCmdChannel();
		if (cmd_channel)
		{
			return cmd_channel->sendCommand(receiver, strCmd);
		}
	}
	return ZoomVideoSDKErrors_Uninitialize;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::SendChatToAll(const zchar_t* msgContent)
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKChatHelper* chat_helper = video_sdk_obj_->getChatHelper();
		if (chat_helper)
		{
			return chat_helper->sendChatToAll(msgContent);
		}
	}
	return ZoomVideoSDKErrors_Uninitialize;
}

const zchar_t* ZoomVideoSDKMgr::GetSessionName() const
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKSession* session = video_sdk_obj_->getSessionInfo();
		if (session)
		{
			return session->getSessionName();
		}
	}
	return nullptr;
}

int ZoomVideoSDKMgr::GetUserCountInSession()
{
	if (video_sdk_obj_)
	{
		return static_cast<int>(GetAllUsers().size());
	}
	return 0;
}

bool ZoomVideoSDKMgr::IsInSession()
{
	if (video_sdk_obj_)
	{
		return video_sdk_obj_->isInSession();
	}
	return false;
}

bool ZoomVideoSDKMgr::IsHost()
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKUser* my_self = GetMySelf();
		if (my_self)
		{
			return my_self->isHost();
		}
	}
	return false;
}

IZoomVideoSDKUser* ZoomVideoSDKMgr::GetSharingUser()
{
	if (video_sdk_obj_)
	{
		vector<IZoomVideoSDKUser*> user_list = GetAllUsers();

		for (size_t i = 0; i < user_list.size(); i++)
		{
			IZoomVideoSDKUser* user = user_list[i];
			if (!user) continue;

			ZoomVideoSDKShareStatus share_status = user->getShareStatus();
			if (share_status == ZoomVideoSDKShareStatus_Start || share_status == ZoomVideoSDKShareStatus_Resume)
			{
				return user;
			}
		}
	}
	return nullptr;
}

IZoomVideoSDKUser* ZoomVideoSDKMgr::GetMySelf()
{
	IZoomVideoSDKUser* my_self = nullptr;
	if (video_sdk_obj_)
	{
		IZoomVideoSDKSession* session = video_sdk_obj_->getSessionInfo();
		if (session)
		{
			my_self = session->getMyself();
		}
	}
	return my_self;
}

IZoomVideoSDKUser* ZoomVideoSDKMgr::GetSessionHost()
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKSession* session = video_sdk_obj_->getSessionInfo();
		if (session)
		{
			return session->getSessionHost();
		}
	}
	return NULL;
}

IVideoSDKVector<IZoomVideoSDKCameraDevice*>* ZoomVideoSDKMgr::GetCameraList()
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKVideoHelper* video_helper = video_sdk_obj_->getVideoHelper();
		if (video_helper)
		{
			return video_helper->getCameraList();
		}
	}
	return nullptr;
}

IVideoSDKVector<IZoomVideoSDKSpeakerDevice*>* ZoomVideoSDKMgr::GetSpeakerList()
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKAudioHelper* audio_helper = video_sdk_obj_->getAudioHelper();
		if (audio_helper)
		{
			return audio_helper->getSpeakerList();
		}
	}
	return nullptr;
}

IVideoSDKVector<IZoomVideoSDKMicDevice*>* ZoomVideoSDKMgr::GetMicList()
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKAudioHelper* audio_helper = video_sdk_obj_->getAudioHelper();
		if (audio_helper)
		{
			return audio_helper->getMicList();
		}
	}
	return nullptr;
}

vector<IZoomVideoSDKUser*> ZoomVideoSDKMgr::GetAllUsers()
{
	vector<IZoomVideoSDKUser*> vect_all_user;

	if (video_sdk_obj_)
	{
		IZoomVideoSDKSession* session = video_sdk_obj_->getSessionInfo();
		if (session)
		{
			IZoomVideoSDKUser* my_self = session->getMyself();
			if (my_self)
			{
				vect_all_user.push_back(my_self);
			}

			IVideoSDKVector<IZoomVideoSDKUser*>* remote_users = session->getRemoteUsers();

			if (remote_users)
			{
				for (int i = 0; i < remote_users->GetCount(); i++)
				{
					IZoomVideoSDKUser* user = remote_users->GetItem(i);
					if (!user) continue;
					vect_all_user.push_back(user);
				}
			}
		}
	}
	return vect_all_user;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::GetSessionAudioStatisticInfo(ZoomVideoSDKSessionAudioStatisticInfo& send_info, ZoomVideoSDKSessionAudioStatisticInfo& recv_info)
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKSession* session = video_sdk_obj_->getSessionInfo();
		if (session)
		{
			session->getSessionAudioStatisticInfo(send_info, recv_info);
		}
	}

	return ZoomVideoSDKErrors_Uninitialize;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::GetSessionVideoStatisticInfo(ZoomVideoSDKSessionASVStatisticInfo& send_info, ZoomVideoSDKSessionASVStatisticInfo& recv_info)
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKSession* session = video_sdk_obj_->getSessionInfo();
		if (session)
		{
			session->getSessionVideoStatisticInfo(send_info, recv_info);
		}
	}

	return ZoomVideoSDKErrors_Uninitialize;
}

ZoomVideoSDKErrors ZoomVideoSDKMgr::GetSessionShareStatisticInfo(ZoomVideoSDKSessionASVStatisticInfo& send_info, ZoomVideoSDKSessionASVStatisticInfo& recv_info)
{
	if (video_sdk_obj_)
	{
		IZoomVideoSDKSession* session = video_sdk_obj_->getSessionInfo();
		if (session)
		{
			session->getSessionShareStatisticInfo(send_info, recv_info);
		}
	}

	return ZoomVideoSDKErrors_Uninitialize;
}

wstring ZoomVideoSDKMgr::GetErrorStringByErrorCode(ZoomVideoSDKErrors err)
{
	return ParseVideoSDKError(err);
}
