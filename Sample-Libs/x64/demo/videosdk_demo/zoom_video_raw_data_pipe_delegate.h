#pragma once

#include "zoom_video_sdk_delegate.h"

namespace zoom_video_api {
	using namespace ZOOMVIDEOSDK;
	
	class zoom_video_raw_data_pipe_delegate:
		public IZoomVideoSDKRawDataPipeDelegate
	{
	public:
		zoom_video_raw_data_pipe_delegate();
		~zoom_video_raw_data_pipe_delegate() override;
		
		virtual void Subscribe(IZoomVideoSDKUser* pUser, ZoomVideoSDKRawDataType dataType, ZoomVideoSDKResolution eResolution);
		void unSubscribe();
		
		void Clean();

		bool GetUserHasSubscribed();

	protected:
		IZoomVideoSDKUser* user_;
		ZoomVideoSDKRawDataType raw_data_type_;
		
		bool user_has_subscribed_;
		bool video_frame_received_;

	// IZoomVideoSDKRawDataPipeDelegate
	public: 
		virtual void onRawDataFrameReceived(YUVRawDataI420* data);
		virtual void onRawDataStatusChanged(RawDataStatus status);
	};
}
