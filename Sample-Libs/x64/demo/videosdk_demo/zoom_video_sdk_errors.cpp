#include "zoom_video_sdk_errors.h"

#include "zoom_video_sdk_def.h"

namespace ZOOMVIDEOSDK {
	const wchar_t* ParseVideoSDKError(int error) {
		switch(error) {
			#define ZOOMVIDEOSSERKERRORS_X(error, name) case error: return name;
			ZOOMVIDEOSDKERRORS
			#undef ZOOMVIDEOSSERKERRORS_X
			default: break;
		}
		return L"UNDEFINED";
	}
}
