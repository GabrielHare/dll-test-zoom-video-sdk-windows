#pragma once

// Copied from zoom_video_sdk_def.h
// NOTE: This could be broken into sections to support enumeration offset
#define ZOOMVIDEOSDKERRORS \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Success, L"Success") /* = 0 */\
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Wrong_Usage, L"Wrong_Usage") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Internal_Error, L"Internal_Error") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Uninitialize, L"Uninitialize") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Memory_Error, L"Memory_Error") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Load_Module_Error, L"Load_Module_Error") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_UnLoad_Module_Error, L"UnLoad_Module_Error") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Invalid_Parameter, L"Invalid_Parameter") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Call_Too_Frequently, L"Call_Too_Frequently") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_No_Impl, L"No_Impl") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Dont_Support_Feature, L"Dont_Support_Feature") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Unknown, L"Unknown") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Auth_Error, L"Auth_Error") /* = 1001 */\
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Auth_Empty_Key_or_Secret, L"Auth_Empty_Key_or_Secret") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Auth_Wrong_Key_or_Secret, L"Auth_Wrong_Key_or_Secret") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Auth_DoesNot_Support_SDK, L"Auth_DoesNot_Support_SDK") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Auth_Disable_SDK, L"Auth_Disable_SDK") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_JoinSession_NoSessionName, L"JoinSession_NoSessionName") /* = 1500 */\
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_JoinSession_NoSessionToken, L"JoinSession_NoSessionToken") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_JoinSession_NoUserName, L"JoinSession_NoUserName") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_JoinSession_Invalid_SessionName, L"JoinSession_Invalid_SessionName") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_JoinSession_Invalid_Password, L"JoinSession_Invalid_Password") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_JoinSession_Invalid_SessionToken, L"JoinSession_Invalid_SessionToken") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_JoinSession_SessionName_TooLong, L"JoinSession_SessionName_TooLong") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_JoinSession_Token_MismatchedSessionName, L"JoinSession_Token_MismatchedSessionName") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_JoinSession_Token_NoSessionName, L"JoinSession_Token_NoSessionName") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_JoinSession_Token_RoleType_EmptyOrWrong, L"JoinSession_Token_RoleType_EmptyOrWrong") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_JoinSession_Token_UserIdentity_TooLong, L"JoinSession_Token_UserIdentity_TooLong") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_SessionModule_Not_Found, L"SessionModule_Not_Found") /* = 2001 */\
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_SessionService_Invalid, L"SessionService_Invalid") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Join_Failed, L"Session_Join_Failed") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_No_Rights, L"Session_No_Rights") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Already_In_Progress, L"Session_Already_In_Progress") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Dont_Support_SessionType, L"Session_Dont_Support_SessionType") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Reconnecting, L"Session_Reconnecting") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Disconnecting, L"Session_Disconnecting") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Not_Started, L"Session_Not_Started") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Need_Password, L"Session_Need_Password") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Password_Wrong, L"Session_Password_Wrong") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Remote_DB_Error, L"Session_Remote_DB_Error") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Invalid_Param, L"Session_Invalid_Param") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Client_Incompatible, L"Session_Client_Incompatible") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Audio_Error, L"Session_Audio_Error") /* = 3000 */\
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Audio_No_Microphone, L"Session_Audio_No_Microphone") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Audio_No_Speaker, L"Session_Audio_No_Speaker") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Video_Error, L"Session_Video_Error") /* = 4000 */\
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Video_Device_Error, L"Session_Video_Device_Error") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Live_Stream_Error, L"Session_Live_Stream_Error") /* = 5000 */\
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Phone_Error, L"Session_Phone_Error") /* = 5500 */\
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_MALLOC_FAILED, L"RAWDATA_MALLOC_FAILED") /* = 6001 */\
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_NOT_IN_Session, L"RAWDATA_NOT_IN_Session") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_NO_LICENSE, L"RAWDATA_NO_LICENSE") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_VIDEO_MODULE_NOT_READY, L"RAWDATA_VIDEO_MODULE_NOT_READY") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_VIDEO_MODULE_ERROR, L"RAWDATA_VIDEO_MODULE_ERROR") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_VIDEO_DEVICE_ERROR, L"RAWDATA_VIDEO_DEVICE_ERROR") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_NO_VIDEO_DATA, L"RAWDATA_NO_VIDEO_DATA") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_SHARE_MODULE_NOT_READY, L"RAWDATA_SHARE_MODULE_NOT_READY") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_SHARE_MODULE_ERROR, L"RAWDATA_SHARE_MODULE_ERROR") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_NO_SHARE_DATA, L"RAWDATA_NO_SHARE_DATA") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_AUDIO_MODULE_NOT_READY, L"RAWDATA_AUDIO_MODULE_NOT_READY") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_AUDIO_MODULE_ERROR, L"RAWDATA_AUDIO_MODULE_ERROR") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_NO_AUDIO_DATA, L"RAWDATA_NO_AUDIO_DATA") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_PREPROCESS_RAWDATA_ERROR, L"RAWDATA_PREPROCESS_RAWDATA_ERROR") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_NO_DEVICE_RUNNING, L"RAWDATA_NO_DEVICE_RUNNING") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_INIT_DEVICE, L"RAWDATA_INIT_DEVICE") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_VIRTUAL_DEVICE, L"RAWDATA_VIRTUAL_DEVICE") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_CANNOT_CHANGE_VIRTUAL_DEVICE_IN_PREVIEW, L"RAWDATA_CANNOT_CHANGE_VIRTUAL_DEVICE_IN_PREVIEW") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_INTERNAL_ERROR, L"RAWDATA_INTERNAL_ERROR") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_SEND_TOO_MUCH_DATA_IN_SINGLE_TIME, L"RAWDATA_SEND_TOO_MUCH_DATA_IN_SINGLE_TIME") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_SEND_TOO_FREQUENTLY, L"RAWDATA_SEND_TOO_FREQUENTLY") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_RAWDATA_VIRTUAL_MIC_IS_TERMINATE, L"RAWDATA_VIRTUAL_MIC_IS_TERMINATE") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Share_Error, L"Session_Share_Error") /* = 7001 */\
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Share_Module_Not_Ready, L"Session_Share_Module_Not_Ready") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Share_You_Are_Not_Sharing, L"Session_Share_You_Are_Not_Sharing") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Share_Type_Is_Not_Support, L"Session_Share_Type_Is_Not_Support") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Session_Share_Internal_Error, L"Session_Share_Internal_Error") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Dont_Support_Multi_Stream_Video_User, L"Dont_Support_Multi_Stream_Video_User") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Fail_Assign_User_Privilege, L"Fail_Assign_User_Privilege") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_No_Recording_In_Process, L"No_Recording_In_Process") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Set_Virtual_Background_Fail, L"Set_Virtual_Background_Fail") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Filetransfer_UnknowError, L"Filetransfer_UnknowError") /* = 7500 */\
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Filetransfer_FileTypeBlocked, L"Filetransfer_FileTypeBlocked") \
	ZOOMVIDEOSSERKERRORS_X(ZoomVideoSDKErrors_Filetransfer_FileSizelimited, L"Filetransfer_FileSizelimited")

namespace ZOOMVIDEOSDK {
	/// \brief Convert error enum value to variable name string
	const wchar_t* ParseVideoSDKError(int error);
}
