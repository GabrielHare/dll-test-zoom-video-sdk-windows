#include "zoom_video_api.h"

#include <string>
#include <iostream>

int main(int argc, char* argv[])
{
	auto success = zoom_video_api::start();
	if (success)
	{
		std::cout << "Zoom started" << std::endl;
	}
	else
	{
		std::cout << "Zoom failed to start" << std::endl;
		return - 1;
	}

	auto user_name = L"TestUser1";
	auto session_name = L"TestSession";
	auto session_password = L""; // WARING: this does not work if not empty!

	std::wstring jwt;
	std::cout << "Enter jwt:\n" << std::endl;
	std::wcin >> jwt;
	
	// Join a session
	success = zoom_video_api::join(user_name, session_name, session_password, jwt.data());
	if (success)
	{
		std::cout << "Zoom connecting..." << std::endl;
	}
	else
	{
		std::cout << "Zoom failed to begin connecting" << std::endl;
		zoom_video_api::leave();
		zoom_video_api::stop();
		return - 1;
	}
        
	std::cout << "Hit Enter to Exit" << std::endl;
	std::string exit;
	std::cin >> exit;
        
	zoom_video_api::leave();
	zoom_video_api::stop();
	
	return 0;
}
