#include "zoom_video_sdk_delegate.h"

#include "videosdk_demo_mgr.h"
#include "zoom_video_sdk_errors.h"
#include "zoom_video_sdk_password_handler_interface.h"


namespace zoom_video_api {
	using namespace ZOOMVIDEOSDK;

	zoom_video_sdk_delegate& zoom_video_sdk_delegate::GetInstance()
	{
		static zoom_video_sdk_delegate instance;
		return instance;
	}
	
	zoom_video_sdk_delegate::zoom_video_sdk_delegate()
	{
	}

	zoom_video_sdk_delegate::~zoom_video_sdk_delegate()
	{
		UninitVideoSDK();
	}

	void zoom_video_sdk_delegate::InitVideoSDK() {
		ZoomVideoSDKInitParams init_params;
		init_params.domain = L"https://go.zoom.us";	
		init_params.enableLog = true;
		init_params.logFilePrefix = L"zoom_video";
		init_params.videoRawDataMemoryMode = ZoomVideoSDKRawDataMemoryModeHeap;
		init_params.shareRawDataMemoryMode = ZoomVideoSDKRawDataMemoryModeHeap;
		init_params.audioRawDataMemoryMode = ZoomVideoSDKRawDataMemoryModeHeap;
		init_params.enableIndirectRawdata = false;

		ZoomVideoSDKMgr::GetInst().Init(this, init_params);
	}

	void zoom_video_sdk_delegate::JoinSession(ZoomVideoSDKSessionContext& session_context) {
		IZoomVideoSDKSession* pSession = ZoomVideoSDKMgr::GetInst().JoinSession(session_context);
		if (pSession)
		{
			// Demo displays "connecting..." header at this stage
			std::wcout << "zoom_video: connecting..." << std::endl;
		}
	}

	void zoom_video_sdk_delegate::LeaveSession(bool end) {
		ZoomVideoSDKErrors err = ZoomVideoSDKMgr::GetInst().LeaveSession(end);
		std::wcout << "zoom_video: disconnecting..." << std::endl;
	}

	void zoom_video_sdk_delegate::UninitVideoSDK() {
		ZoomVideoSDKMgr::GetInst().UnInit();
	}

	// IZoomVideoSDKDelegate

	void zoom_video_sdk_delegate::onError(ZoomVideoSDKErrors errorCode, int detailErrorCode) {
		std::wcout << "zoom_video: onError('" << ParseVideoSDKError(errorCode) << "', '" << detailErrorCode << "')" << std::endl;
	}

	void zoom_video_sdk_delegate::onSessionJoin() {
		bool isHost = ZoomVideoSDKMgr::GetInst().IsHost();
		const wchar_t* szSessionName = ZoomVideoSDKMgr::GetInst().GetSessionName();
		std::wcout << "zoom_video: onSessionJoin, is host = '" <<  isHost << "', session name = '" << szSessionName << "'"  << std::endl;
	}

	void zoom_video_sdk_delegate::onSessionLeave() {
		std::wcout << "zoom_video: onSessionLeave" << std::endl;
	}
	
	void zoom_video_sdk_delegate::onSessionNeedPassword(IZoomVideoSDKPasswordHandler* handler) {
		std::wcout << "zoom_video: onSessionNeedPassword" << std::endl;
		
		if (!handler)
			return;
		
		handler->leaveSessionIgnorePassword();
	}
	
	void zoom_video_sdk_delegate::onSessionPasswordWrong(IZoomVideoSDKPasswordHandler* handler) {
		std::wcout << "zoom_video: onSessionPasswordWrong" << std::endl;
		
		if (!handler)
			return;
		
		handler->leaveSessionIgnorePassword();
	}
}
