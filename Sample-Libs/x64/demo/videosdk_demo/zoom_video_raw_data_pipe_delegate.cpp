#include "zoom_video_raw_data_pipe_delegate.h"

#include "videosdk_demo_mgr.h"

namespace zoom_video_api {
	using namespace ZOOMVIDEOSDK;
	
	zoom_video_raw_data_pipe_delegate::zoom_video_raw_data_pipe_delegate():
		user_(nullptr),
		raw_data_type_(RAW_DATA_TYPE_VIDEO),
		user_has_subscribed_(false),
		video_frame_received_(false)
	{
	}

	zoom_video_raw_data_pipe_delegate::~zoom_video_raw_data_pipe_delegate()
	{
		Clean();
	}

	
	void zoom_video_raw_data_pipe_delegate::Subscribe(IZoomVideoSDKUser* pUser, ZoomVideoSDKRawDataType dataType, ZoomVideoSDKResolution eResolution)
	{
		if (!pUser) return;

		ZoomVideoSDKErrors err = ZoomVideoSDKErrors_Unknown;
		IZoomVideoSDKRawDataPipe* pPipe = nullptr;

		if (dataType == RAW_DATA_TYPE_VIDEO)
		{
			pPipe = pUser->GetVideoPipe();
		}
		else if (dataType == RAW_DATA_TYPE_SHARE)
		{
			pPipe = pUser->GetSharePipe();
		}

		if (pPipe)
		{
			err = pPipe->subscribe(eResolution, this);
		}

		video_frame_received_ = false;

		if (err == ZoomVideoSDKErrors_Success)
		{
			user_ = pUser;
			raw_data_type_ = dataType;
			user_has_subscribed_ = true;
		}
		else if (err == ZoomVideoSDKErrors_RAWDATA_VIDEO_DEVICE_ERROR)
		{
			// QUESTION: Why is this done in the case of failure??
			user_ = pUser;
		}
	}

	void zoom_video_raw_data_pipe_delegate::unSubscribe()
	{
		if (!user_)
		{
			return;
		}

		ZoomVideoSDKErrors err = ZoomVideoSDKErrors_Unknown;

		switch (raw_data_type_)
		{
		case RAW_DATA_TYPE_VIDEO:
		{
			if (user_->GetVideoPipe())
			{
				err = user_->GetVideoPipe()->unSubscribe(this);
			}
		}
			break;
		case RAW_DATA_TYPE_SHARE:
		{
			if (user_->GetSharePipe())
			{
				err = user_->GetSharePipe()->unSubscribe(this);
			}
		}
			break;
		default:
			break;
		}

		Clean();
	}

	void zoom_video_raw_data_pipe_delegate::Clean()
	{
		user_ = nullptr;
		user_has_subscribed_ = false;
		video_frame_received_ = false;
	}

	// IZoomVideoSDKRawDataPipeDelegate
	
	void zoom_video_raw_data_pipe_delegate::onRawDataFrameReceived(YUVRawDataI420* data)
	{
		if(video_frame_received_) return;
		video_frame_received_ = true;
		
		// Get frame data resolution.
		auto width = data->GetStreamWidth();
		auto height = data->GetStreamHeight();
		
		std::wcout << "zoom_video: onRawDataFrameReceived width = " << width << ", height = " << height << std::endl;

		// Get frame rotation
		// NOTE: enum is defined in videosdk_demo_def.h
		//auto rotation = data->GetRotation();

		// Get frame buffer.
		/*
		data->GetYBuffer();
		data->GetUBuffer();
		data->GetVBuffer();
		*/
	}

	void zoom_video_raw_data_pipe_delegate::onRawDataStatusChanged(RawDataStatus status)
	{
		switch (status) {
		case RawData_On:
			std::cout << "zoom_video: onRawDataStatusChanged(RawData_On)" << std::endl;
			break;
		case RawData_Off:
			std::cout << "zoom_video: onRawDataStatusChanged(RawData_Off)" << std::endl;
			break;
		}
	}

	bool zoom_video_raw_data_pipe_delegate::GetUserHasSubscribed()
	{
		return user_has_subscribed_;
	}

}
