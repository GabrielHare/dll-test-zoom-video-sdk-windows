﻿using System.Runtime.InteropServices;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.Json;

public static class Program
{
    [DllImport("zoomvideo")]
    [return: MarshalAs(UnmanagedType.I1)]
    private static extern bool start();

    [DllImport("zoomvideo")]
    [return: MarshalAs(UnmanagedType.I1)]
    private static extern bool join(
        [MarshalAs(UnmanagedType.LPWStr)] string user_name, 
        [MarshalAs(UnmanagedType.LPWStr)] string session_name, 
        [MarshalAs(UnmanagedType.LPWStr)] string session_password, 
        [MarshalAs(UnmanagedType.LPWStr)] string jwt);
    
    [DllImport("zoomvideo")]
    private static extern void leave();
    
    [DllImport("zoomvideo")]
    private static extern void stop();

    [Serializable]
    public class JWTResponse
    {
        public string signature;
    }

    private static DataContractJsonSerializer _serializer = new (typeof(JWTResponse));
    
    public static void Main(string[] args)
    {
        var success = start();
        if (success)
        {
            Console.WriteLine("Zoom started");
        }
        else
        {
            Console.WriteLine("Zoom failed to start");
            return;
        }

        var user_name = "TestUser1";
        var session_name = "TestSession";
        var session_password = ""; // WARING: this does not work if not empty!
        
        // Request JWT from local service
        // NOTE: Service source code is here: https://github.com/zoom/videosdk-auth-endpoint-sample
        using StringContent jsonContent = new(
            JsonSerializer.Serialize(new
            {
                role = 1, // Identify as host
                sessionName = session_name,
                sessionKey = "", // IMPORTANT: This is NOT the password
                userIdentity = user_name
            }),
            Encoding.UTF8,
            "application/json");
        HttpClient client = new HttpClient();
        var response = client.PostAsync("http://localhost:4000", jsonContent).Result;
        // var responseString = response.Content.ReadAsStringAsync().Result;
        // Console.WriteLine($"Received JSON:\n{responseString}");
        var responseStream = response.Content.ReadAsStreamAsync().Result;
        var responseInstance = _serializer.ReadObject(responseStream) as JWTResponse;
        var jwt = responseInstance.signature;
        Console.WriteLine($"Received JWT:\n{jwt}");
        
        // Join a session
        success = join(user_name, session_name, session_password, jwt);
        if (success)
        {
            Console.WriteLine("Zoom connecting...");
        }
        else
        {
            Console.WriteLine("Zoom failed to begin connecting");
            leave();
            stop();
            return;
        }
        
        Console.WriteLine("Hit Enter to Exit");
        var name = Console.ReadLine();
        
        leave();
        stop();
    }
}
