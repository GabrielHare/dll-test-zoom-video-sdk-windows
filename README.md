# Zoom Video SDK FORK INFO

This is just a fork of the existing Windows Video SDK, which can be downloaded here: https://developers.zoom.us/docs/video-sdk/developer-accounts/#download-video-sdk

The main demo has been modified to support debugging in release builds, but otherwise still works as expected.

Additionally, two projects have been added to create a dll and test it.

**WARNING:** These projects are not yet functional!

## Testing the DLL Project

### (0) Set up the video-auth-endpoint-sample project
`git clone https://github.com/zoom/videosdk-auth-endpoint-sample.git`

Follow direction in README.md

Generate a Video SDK JWT to join Zoom Video SDK sessions with the Video SDK.

### (1) Open solution for this repository
The solution path is: `dll-test-zoom-video-sdk-windows\Sample-Libs\x64\demo\videosdk_demo\videosdk_demo.sln`

The solution contains 3 projects:
- videosdk_demo - the original demo
- zoomvideo - a minimal dll packaging
- videosdk_console - a console app to test the dll

### (2) Build zoomvideo and then videosdk_console for “Release x64”

This release build configuration for all projects has been modified to support debugging breakpoints.

### (3) Get a web token
Launch the endpoint, then post the following request:
```
curl --header "Content-Type: application/json" \
--request POST \
--data '{"sessionName": "TestSession", "role": 1, "sessionKey": "", "userIdentity": "TestUser1"}' \
http://localhost:4000
```

### (4) Run the videosdk_console app

When asked to “Enter jwt” past the web token from the endpoint response.

The following steps will be logged:
```
zoom_video: connecting...
zoom_video: subscribing 1 users
zoom_video: subscribing user 'TestUser1'
Zoom connecting...
Hit Enter to Exit
zoom_video: onRawDataFrameReceived width = 1920, height = 1080
```

**PROBLEM:** The expected message zoom_video: onSessionJoin, is host = '1', session name = 'testsession' is never received.

However, if the demo application is run, the expected message will be printed to the console



# Zoom Windows Video SDK

## Documentation
Please visit [Zoom Windows Video SDK](https://developers.zoom.us/docs/video-sdk/windows/) to learn how to use the SDK and run the sample application.

## Changelog

For the changelog, see [Video SDK - Windows](https://devsupport.zoom.us/hc/en-us/sections/9481996074637-Windows).

## Support

For any issues regarding our SDK, please visit our Developer Support Forum at https://devforum.zoom.us/.

## License

Use of this SDK is subject to our [License and Terms of Use](https://explore.zoom.us/docs/en-us/video-sdk-terms.html);

---
Copyright ©2023 Zoom Video Communications, Inc. All rights reserved.
